# tarea12_sqlserver

# -- Total de ventas en el año 2009:
# --1. ¿Cuál es el total de ventas realizadas en el año 2009?

~~~
DECLARE @fecha_inicio datetime = '2009-01-01';
DECLARE @fecha_fin datetime = '2009-12-31';

SELECT SUM(total) AS Total_Ventas
FROM [ve].[documento]
WHERE fechaMovimiento BETWEEN @fecha_inicio AND @fecha_fin;
~~~

# --Personas sin entradas registradas en la tabla personaDestino:

# -- 2. ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino?
~~~
SELECT p.*
FROM [ma].[persona] p
LEFT JOIN [ma].[personaDestino] pd ON p.persona = pd.persona
WHERE pd.persona IS NULL;
~~~

# --Promedio del monto total de transacciones de ventas:

# -- 3. ¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos, expresado en moneda local (soles peruanos)?
~~~
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM [ve].[documento];
~~~ 

# -- Documentos de ventas con monto total superior al promedio:


# -- 4. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.
~~~ 
SELECT *
FROM [ve].[documento]
WHERE total > (SELECT AVG(total) FROM [ve].[documento]);
~~~ 

# -- Documentos de ventas pagados con una forma de pago específica:

# -- 5. Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde [ve].[documentoPago]
~~~
SELECT d.*
FROM [ve].[documentoPago] dp
INNER JOIN [ve].[documento] d ON dp.documento = d.documento
INNER JOIN [pa].[pago] p ON dp.pago = p.pago
WHERE p.formaPago = 1; -- 1 es pago al Contado
~~~

# -- Detalles de documentos de ventas canjeados:

# -- 6. ¿Cuál es la consulta que devuelve los detalles de los documentos de ventas que han sido canjeados, considerando que un documento puede ser canjeado por uno o más documentos, y estos detalles deben incluir información de los documentos originales y los documentos canjeados?
~~~
SELECT d.*, dc.*
FROM [ve].[documento] d
INNER JOIN [ve].[documentosCanjeados] dc ON d.documento = dc.documento01 OR d.documento = dc.documento02;
~~~

# -- Saldo total distribuido por almacén:

# -- 7. "¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos?"
~~~
SELECT almacen, SUM(costoSoles) AS Saldo_Total
FROM [ma].[saldosIniciales]
GROUP BY almacen;
~~~

# --Detalles de documentos de ventas por vendedor:

# -- 8. ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos, considerando la información detallada de cada documento en relación con sus elementos de venta?
~~~
SELECT d.*
FROM [ve].[documento] d
INNER JOIN [ve].[documentoDetalle] dd ON d.documento = dd.documento
WHERE d.vendedor = 3;
~~~

# --Total de ventas por año y vendedor:

# -- 9. ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en un año específico sea superior a 100,000 unidades monetarias?
~~~
SELECT vendedor, YEAR(fechaMovimiento) AS Anio, SUM(total) AS Total_Ventas
FROM ve.documento
GROUP BY vendedor, YEAR(fechaMovimiento)
HAVING SUM(total) > 100000;
~~~

# -- Desglose mensual de ventas por vendedor:

# -- 10. ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico?
~~~
SELECT vendedor, MONTH(fechaMovimiento) AS Anio, SUM(total) AS Total_Ventas
FROM ve.documento
GROUP BY vendedor, MONTH(fechaMovimiento)
HAVING SUM(total) > 100000;
~~~

# --Clientes que compraron más de 10 veces en un año:

# -- 11. ¿Cuántos clientes compraron más de 10 veces en un año?
~~~
SELECT persona, YEAR(fechaMovimiento) AS Año, COUNT(*) AS Compras
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING COUNT(*) > 10;
~~~


# --Total acumulado de descuentos por vendedor:

# -- 12. ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000?
~~~
SELECT vendedor, SUM(descto01 + descto02 + descto03) AS Descuentos_Acumulados
FROM ve.documento
GROUP BY vendedor
HAVING SUM(descto01 + descto02 + descto03) > 5000;
~~~

# --Total anual de ventas por persona:

# -- 13. ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000?
~~~
SELECT persona, YEAR(fechaMovimiento) AS Año, SUM(total) AS Total_Anual
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000;
~~~

# --Recuento total de productos vendidos por vendedor:

# -- 14. ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas?
~~~
SELECT 
    d.vendedor, 
    COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM 
    ve.documentoDetalle dd
JOIN 
    ve.documento d ON dd.documento = d.documento
GROUP BY 
    d.vendedor;
~~~

# --Ventas mensuales desglosadas por tipo de pago:

# --15. ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?
~~~
SELECT 
    MONTH(d.fechaMovimiento) AS Mes,
    p.formaPago,
    SUM(d.total) AS Total_Ventas
FROM 
    ve.documento d
JOIN 
    pa.pago p ON d.vendedor = p.vendedor
WHERE 
    YEAR(d.fechaMovimiento) = 2009
GROUP BY 
    MONTH(d.fechaMovimiento),
    p.formaPago;
~~~

# --Total de ventas en el año 2007:

# --16¿Cuál es el total de ventas realizadas en el año 2007?
~~~
SELECT SUM(total) AS Total_Ventas_2007
FROM [ve].[documento]
WHERE YEAR(fechaMovimiento) = 2007;
~~~

# --Personas sin entradas registradas en la tabla personaDestino en el año 2008:

# --17¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino en el año 2008?
~~~
SELECT p.*
FROM [ma].[persona] p
LEFT JOIN (
    SELECT persona 
    FROM [ma].[personaDestino]
    WHERE YEAR(fechaMovimiento) = 2008
) pd ON p.persona = pd.persona
WHERE pd.persona IS NULL;

EXEC sp_help '[ma].[personaDestino]';

SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'personaDestino';
~~~

select * from ma.persona;

# --Promedio del monto total de transacciones de ventas en el año 2009:

# --18¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos en el año 2009, expresado en moneda local (soles peruanos)?
~~~
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS Promedio_Monto_Total_2009
FROM [ve].[documento]
WHERE YEAR(fechaMovimiento) = 2009;
~~~

# --Documentos de ventas con monto total superior al promedio en el año 2005:

# --19 Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2005.
~~~
SELECT *
FROM [ve].[documento]
WHERE total > (SELECT AVG(total) FROM [ve].[documento] WHERE YEAR(fechaMovimiento) = 2005);
~~~

# --Documentos de ventas pagados con una forma de pago específica en el año 2006:

# --20Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde la tabla documentoPago en el año 2006
~~~
DECLARE @formaPago INT = 1;

SELECT d.*
FROM [ve].[documentoPago] dp
INNER JOIN [ve].[documento] d ON dp.documento = d.documento
INNER JOIN [pa].[pago] p ON dp.pago = p.pago
WHERE p.formaPago = @formaPago
  AND YEAR(d.fechaMovimiento) = 2006;

select * from ve.documentoPago

SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME IN ('documentoPago', 'documento');
~~~


# --Detalles de documentos de ventas canjeados en el año 2007:

# --21 ¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos en el año 2007?
~~~
SELECT d.*, dc.*
FROM [ve].[documento] d
INNER JOIN [ve].[documentosCanjeados] dc ON d.documento = dc.documento01 OR d.documento = dc.documento02
WHERE YEAR(d.fechaMovimiento) = 2007;
~~~

# --Saldo total distribuido por almacén en el año 2008:

# --22 Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2008.

~~~
DECLARE @anio INT = 2008;

SELECT *
FROM [ve].[documento]
WHERE YEAR(fechaMovimiento) = @anio
  AND total > (SELECT AVG(total) 
               FROM [ve].[documento] 
               WHERE YEAR(fechaMovimiento) = @anio);
~~~

# --Detalles de documentos de ventas por vendedor en el año 2009:

# --23 ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos en el año 2009, considerando la información detallada de cada documento en relación con sus elementos de venta?
~~~
SELECT vendedor, MONTH(fechaMovimiento) AS Mes, SUM(total) AS Total_Ventas
FROM ve.documento
WHERE YEAR(fechaMovimiento) = 2009
GROUP BY vendedor, MONTH(fechaMovimiento);
~~~


# --Total de ventas por año y vendedor en el año 2008:

# --24 ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en el año 2008 sea superior a 100,000 unidades monetarias?
~~~
SELECT vendedor, YEAR(fechaMovimiento) AS Anio, SUM(total) AS Total_Ventas
FROM ve.documento
WHERE YEAR(fechaMovimiento) = 2008
GROUP BY vendedor, YEAR(fechaMovimiento)
HAVING SUM(total) > 100000;
~~~

# --Desglose mensual de ventas por vendedor en el año 2009:

# --25 ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico en el año 2009?
~~~
SELECT vendedor, MONTH(fechaMovimiento) AS Mes, SUM(total) AS Total_Ventas
FROM ve.documento
WHERE YEAR(fechaMovimiento) = 2009
GROUP BY vendedor, MONTH(fechaMovimiento);
~~~


# --Clientes que compraron más de 10 veces en un año en el año 2005:

# --26 ¿Cuántos clientes compraron más de 10 veces en un año en el año 2005?
~~~
SELECT persona, YEAR(fechaMovimiento) AS Anio, COUNT(*) AS Compras
FROM ve.documento
WHERE tipoMovimiento = 1 AND YEAR(fechaMovimiento) = 2005
GROUP BY persona, YEAR(fechaMovimiento)
HAVING COUNT(*) > 10;
~~~

# --Total acumulado de descuentos por vendedor en el año 2006:

# --27 ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000 en el año 2005?
~~~
SELECT vendedor, SUM(descto01 + descto02 + descto03) AS Descuentos_Acumulados
FROM ve.documento
WHERE YEAR(fechaMovimiento) = 2006
GROUP BY vendedor
HAVING SUM(descto01 + descto02 + descto03) > 5000;
~~~
 
# --Total anual de ventas por persona en el año 2007:

# --28 ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000 en el año 2007?
~~~
SELECT persona, YEAR(fechaMovimiento) AS Anio, SUM(total) AS Total_Anual
FROM ve.documento
WHERE tipoMovimiento = 1 AND YEAR(fechaMovimiento) = 2007
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000;
~~~

# --Recuento total de productos vendidos por vendedor en el año 2008:

# --29 ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas en el año 2008?
~~~
SELECT d.vendedor, COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM ve.documentoDetalle dd
JOIN ve.documento d ON dd.documento = d.documento
WHERE YEAR(d.fechaMovimiento) = 2008
GROUP BY d.vendedor;
~~~

# --Ventas mensuales desglosadas por tipo de pago en el año 2009:

# --30 ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?
~~~
SELECT MONTH(d.fechaMovimiento) AS Mes, p.formaPago, SUM(d.total) AS Total_Ventas
FROM ve.documento d
JOIN pa.pago p ON d.vendedor = p.vendedor
WHERE YEAR(d.fechaMovimiento) = 2009
GROUP BY MONTH(d.fechaMovimiento), p.formaPago;
~~~